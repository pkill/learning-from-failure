# Learning From Failure: Why a Total Site Outage Can be a Good Thing

## Thank you for your attention!
Alex Elman

Twitter: [_pkill](https://twitter.com/_pkill)

https://conferences.oreilly.com/velocity/vl-ca/public/schedule/detail/74818

We're hiring! https://go.indeed.com/sre

## Talk outline
- Catastrophic failure
  - What does a catastrophic failure look like for your organization?
  - What catastrophe means for Indeed
- Indeed Job Search fault tolerance as it was 3 years ago
  - Pools of servers and graceful degradation
  - Failure myth #1 and Normalcy Bias
- Catastrophe
  - The incident lifecycle and swiss cheese accident model
  - Failure myth #2
  - Resilient artifact distribution
  - Mitigation
  - Recovery
  - Cleanup
  - Resilience Pattern: Circuit Breaker
- Notes on failure
  - Resilience vs Robustness
  - Failure myth #3
  - Failure myth #4
- Incident response
  - Situational awareness and decision making
  - Cognitive biases
  - Incident lifecycle revisited
- The Retrospective process
  - Debriefing
  - Debrief attendees
  - Qualities of effective facilitators
  - Questions to ask during a debrief to foster psychological safety
  - Debriefing tips
  - Avoid counterfactuals
- Stop doing root cause analysis
  - Root cause is problematic. It's imprecise and constrains our view.
  - Five Why's is also a problem
  - Locate contirbuting factors
- Retrospective completion
  - Deliverables
  - Remediation meetings
- Conclusion: We don't (and can't) deeply know our systems
  - Failure is the best opportunity to learn
  - Failure myth #5
  - What can be measured?
  - Key takeaways

## Related Indeed talks
1. VelocityConf 2017 - [Canary in a coal mine: Building infrastructure resiliency with canary data reloads](https://conferences.oreilly.com/velocity/vl-ca-2017/public/schedule/detail/58895) - Ann Kilzer
2. Indeed Engineering Blog - [RAD – How We Replicate Terabytes of Data Around the World Every Day](https://engineering.indeedblog.com/talks/rad-how-we-replicate-terabytes-of-data-around-the-world-every-day/) - Julie Scully, Jason Koppe
3. Indeed Engineering Blog - [Redundant Array of Inexpensive Datacenters](https://engineering.indeedblog.com/talks/redundant-array-inexpensive-datacenters/) - Chris Graf, Charles Valentine

## References and inspiration

1. [Pre-Accident Investigations](https://www.amazon.com/Pre-Accident-Investigations-Todd-Conklin/dp/1409447820) - Todd Conklin
2. [STELLA report](http://stella.report/)
3. [Etsy's Debriefing facilitation guide](http://extfiles.etsy.com/DebriefingFacilitationGuide.pdf)
4. [The Art of Asking Questions - Etsy's Debriefing Facilitation](https://github.com/etsy/DebriefingFacilitationGuide/blob/master/guide/05-the-art-of-asking-questions.md)
4. [Pagerduty retrospectives](https://postmortems.pagerduty.com/)
5. [Architecting a Technical Post Mortem](https://www.usenix.org/conference/srecon18americas/presentation/gallego) - Will Gallego
5. [No Seriously Root Cause is a Fallacy](https://willgallego.com/2018/04/02/no-seriously-root-cause-is-a-fallacy/) - Will Gallego
6. [The Field Guide to Understanding Human Error](https://books.google.com/books/about/The_Field_Guide_to_Understanding_Human_E.html?id=9DLOyjB1jhsC&printsec=frontcover&source=kp_read_button#v=onepage&q&f=false) - Sidney Dekker
7. [Resilience engineering and error budgets](https://willgallego.com/2019/02/23/resilience-engineering-and-error-budgets/) - Will Gallego
8. [How Complex Systems Fail](https://bit.ly/ComplexFail) - Richard I. Cook, MD
9. [Meltdown: Why complex systems fail and what we can do about it](https://www.amazon.com/Meltdown-Systems-Fail-What-About/dp/0735222630) - Chris Clearfield
10. [Chaos Engineering book](https://learning.oreilly.com/library/view/chaos-engineering/9781491988459/) - Ali Basiri, Nora Jones, Aaron Blohowiak, Lorin Hochstein, Casey Rosenthal
11. [Defining SLOs for services with dependencies - CRE life lessons](https://cloud.google.com/blog/products/gcp/defining-slos-for-services-with-dependencies-cre-life-lessons) - Robert van Gent and Cody Smith
12. [Incidents as we Imagine Them versus How They Actually Are](https://www.adaptivecapacitylabs.com/blog/2018/11/05/incidents-as-we-imagine-them-versus-how-they-actually-are/) - John Allspaw
13. [Google SRE Book](https://landing.google.com/sre/sre-book/toc/index.html)
14. [Normal Accidents: Living with High Risk Technologies](https://press.princeton.edu/titles/6596.html) - Charles Perrow
15. [List of cognitive biases](https://en.wikipedia.org/wiki/List_of_cognitive_biases) - Wikipedia
16. [The Infinite Hows (or, the Dangers of the Five Whys)](https://www.kitchensoap.com/2014/11/14/the-infinite-hows-or-the-dangers-of-the-five-whys/) - John Allspaw
17. [Health Checks and Graceful Degradation in Distributed Systems](https://medium.com/@copyconstruct/health-checks-in-distributed-systems-aa8a0e8c1672) - Cindy Sridharan
18. [Chaos Engineering Traps](https://medium.com/@njones_18523/chaos-engineering-traps-e3486c526059) - Nora Jones
19. [Resilience Engineering Mythbusting](https://youtu.be/td4AIKYr16Q) - Will Gallego
20. [Antics, Drift, and Chaos](https://www.usenix.org/conference/srecon18americas/presentation/hochstein) - Lorin Hochstein
21. [How Did Things Go Right?](https://www.usenix.org/conference/srecon19americas/presentation/kitchens) - Ryan Kitchens
21. [Chaos Engineered or Otherwise is Not Enough](https://medium.com/@jpaulreed/chaos-engineered-or-otherwise-is-not-enough-ad5792309ecf) - J. Paul Reed
22. [Chaos Engineering - What it is, and where it's going](https://www.gremlin.com/blog/adrian-cockroft-chaos-engineering-what-it-is-and-where-its-going-chaos-conf-2018/) - Adrian Cockroft
23. [Chernobyl DevOps: Software Engineering, Disaster Management, and Observability](https://medium.com/@dylnuge/chernobyl-devops-software-engineering-disaster-management-and-observability-8a50a7ea98d6?sk=87ac7c967061ec728f1cebfe821d6e8b) - Dylan Nugent
24. [Simple Testing Can Prevent Most Critical Failures: An Analysis of Production Failures in Distributed Data-Intensive Systems](https://www.usenix.org/system/files/conference/osdi14/osdi14-paper-yuan.pdf) - Ding Yuan, _et. al._
25. [Each necessary, but only jointly sufficient](https://www.kitchensoap.com/2012/02/10/each-necessary-but-only-jointly-sufficient/) - John Allspaw
26. [Dark Debt](https://medium.com/@allspaw/dark-debt-a508adb848dc) - John Allspaw
27. [The Fallacies of Distributed Computing](https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing) - L Peter Deutsch